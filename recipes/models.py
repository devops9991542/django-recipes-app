from django.db import models
from django.contrib.auth.models import User #Importer le model natif de Django (User)
from django.urls import reverse

# Create your models here.

class Recipe(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()

    author = models.ForeignKey(User, on_delete=models.CASCADE) #If we delete a User => all his recipes will be automatically deleted

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse("recipes-detail", kwargs={"pk": self.pk}) #To sendRedirect when creating a new 
    


    def __str__(self):
        return self.title