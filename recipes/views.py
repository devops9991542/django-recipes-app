from django.shortcuts import render, HttpResponse
from recipes import models
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy

# Create your views here.

def home(request):
    recipes = models.Recipe.objects.all()
    context = {
        'recipes':recipes
    }
    return render(request, 'recipes/home.html', context)


class RecipeListView(ListView):
    model = models.Recipe
    template_name = 'recipes/home.html'
    context_object_name = 'recipes' #specifying the context

class RecipeDetailView(DetailView):
    model = models.Recipe



class RecipeCreateView(LoginRequiredMixin, CreateView): #=> Only logged in User can create new Recipe
    model = models.Recipe
    #fields = "__all__" #To get all fields ...
    fields = ['title', 'description']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
    


class RecipeUpdateView(LoginRequiredMixin,UserPassesTestMixin, UpdateView): #=> Only logged in User can create new Recipe
    model = models.Recipe
    #fields = "__all__" #To get all fields ...
    fields = ['title', 'description']

    #Ensure that only owner of a recipe can modify it
    def test_func(self):
        recipe = self.get_object()
        return self.request.user == recipe.author


    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
    


class RecipeDeleteView(LoginRequiredMixin,UserPassesTestMixin, DeleteView): #=> Only logged in User can create new Recipe
    model = models.Recipe
    success_url = reverse_lazy('recipes-home')

    #Ensure that only owner of a recipe can modify it
    def test_func(self):
        recipe = self.get_object()
        return self.request.user == recipe.author



def about(request):
    return render(request, 'recipes/about.html', {'title': 'about us page'}) # To give each page a specific title
